--DISTINCT  One Column

SELECT DISTINCT first_name,last_name FROM STORE_TRANSACTIONS st ;

--DISTINCT Multiple Columns
SELECT DISTINCT  first_name,country FROM STORE_TRANSACTIONS st ;


--DISTINCT WRONG STATEMENT
SELECT st.first_name,DISTINCT st.country FROM STORE_TRANSACTIONS st ;

--Expressions Numeric
SELECT first_name,price,price*1.7201 FROM STORE_TRANSACTIONS ;

--
SELECT first_name,price,count,price*count FROM STORE_TRANSACTIONS;

--
SELECT  (discount/100)*price*count,price*count,price,count,discount,first_name FROM STORE_TRANSACTIONS st ;


--
SELECT  first_name,last_name,price,count,discount,price*count,(discount/100)*price*count,price*count-((discount/100)*price*count) FROM STORE_TRANSACTIONS st ;

--Expressions Date
SELECT first_name,order_date,order_date+7 FROM STORE_TRANSACTIONS st ;

--
SELECT first_name,order_date,order_date-7 FROM STORE_TRANSACTIONS st ;

--
SELECT first_name,order_date,order_date+7,(order_date+7)-order_date FROM STORE_TRANSACTIONS st ;

--Expressions Strings Concatenation
SELECT first_name||last_name,order_date FROM STORE_TRANSACTIONS st ;

--
SELECT first_name||' '||last_name,order_date  FROM STORE_TRANSACTIONS st ;

--
SELECT first_name||' '||last_name||' '||price ,order_date FROM STORE_TRANSACTIONS st ;

-- ALIAS (Alternative Name of column)

SELECT first_name||' '||last_name AS "Customer Name" FROM STORE_TRANSACTIONS st ;

--
SELECT first_name||' '||last_name "Customer Name" FROM STORE_TRANSACTIONS st ;

-- Wrong statement
SELECT first_name||' '||last_name Customer Name FROM STORE_TRANSACTIONS st ;

--
SELECT first_name||' '||last_name customer_name FROM STORE_TRANSACTIONS st ;

--
SELECT  first_name,last_name,price,count,discount,price*count "Amount Witout Discount",(discount/100)*price*count "Discount Amount",price*count-((discount/100)*price*count) "Total Amount" FROM STORE_TRANSACTIONS st ;

--
SELECT  first_name,last_name,price,count,discount||'%' AS "Discount Percent",price*count "Amount Witout Discount",(discount/100)*price*count "Discount Amount",price*count-((discount/100)*price*count) "Total Amount" FROM STORE_TRANSACTIONS st ;


-- WHERE
-- = > < >= <= != <>  between in  is null

--Order By Asc,Desc  nulls first nulls last 

--Group By




