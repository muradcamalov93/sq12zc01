--1.2009-2012 ci illər üzrə, statusu "Bitmiş" olan sifariş nömrələri və ümumi satış üzrə ümumi məhsuldarlığın əldə olunması.

SELECT extract(YEAR FROM order_date) years,
       SUM(sales) sales,
       COUNT(order_status)  AS "Number of Orders"
FROM sales
WHERE order_status = 'Order Finished'
GROUP BY EXTRACT (YEAR FROM order_date);


--2.2011 və 2012 ci illər üzrə alt kateqoriya məhsulları üzrə məhsuldarlıq göstəricilərin müqayisə olunması.

SELECT EXTRACT (YEAR FROM order_date) as years,
       product_sub_category,
       sum(sales) as sales
FROM SALES 
WHERE EXTRACT (YEAR FROM order_date) BETWEEN 2011 AND 2012 AND order_status = 'Order Finished'
GROUP BY EXTRACT (YEAR FROM order_date), product_sub_category
ORDER BY EXTRACT (YEAR FROM order_date), sales DESC;


SELECT sub_category.*,
       ROUND((sales2012-sales2011)*100/sales2012, 1) "sales growth (%)"
FROM(
     SELECT product_sub_category,
            sum(CASE WHEN extract(YEAR FROM ORDER_DATE)=2011 THEN sales ELSE 0 END) sales2011,
            sum(CASE WHEN extract(YEAR FROM order_date)=2012 THEN sales ELSE 0 END) sales2012
     FROM SALES 
     WHERE order_status = 'Order Finished'
     GROUP BY product_sub_category
    ) sub_category


--3.İllər üzrə tanıtım promosyonların istifadə  etmə sürətini hesablamaqla,hal-hazırda aparılan tanıtım promosyonların effektivliyi və nəticəyönümlüyünün müəyyənləşdirilməsi.

SELECT EXTRACT(YEAR FROM order_date) years,
       SUM(sales) sales,
       SUM(discount_value) "promotion_value",
       ROUND( SUM(discount_value)*100/SUM(sales), 2) "burn_rate_percentage"
FROM SALES 
WHERE order_status = 'Order Finished'
GROUP BY EXTRACT(YEAR FROM order_date) ;   

--4.2012 ci il üçün alt kateqoriyalar üzrə aparılmış tanıtım promosyonların istifadə etmə sürətini hesablamaqla, hal-hazırda aparılan tanıtım promosyonların effektivliyi və səmərəliliyinin müəyyənləşdirilməsi.

SELECT extract(YEAR FROM order_date) years,
       product_sub_category,
       product_category,
       SUM(sales) sales,
       SUM(discount_value) promotion_value,
       ROUND(SUM(discount_value)*100/SUM(sales),2) "burn_rate_percentage"

FROM SALES s 
WHERE extract(YEAR FROM order_date)  = 2012
      AND order_status = 'Order Finished'
GROUP BY  product_category,product_sub_category,extract(YEAR FROM order_date)
ORDER BY 4 DESC; 

--5.Hər il üzrə müştəri alışlarının müəyyənləşdirilməsi.

SELECT extract(YEAR FROM order_date) years,
       COUNT(DISTINCT customer) "number of customers"
FROM SALES s 
WHERE order_status = 'Order Finished'
GROUP BY extract(YEAR FROM order_date);

--6.Hər il üzrə yeni cəlb olunan müştəri sayının müəyyənləşdirilməsi.

SELECT EXTRACT(YEAR FROM first_order) years,
       COUNT(customer) "new customer"
FROM(
       SELECT customer,
              MIN(order_date) first_order
       FROM sales
       WHERE order_status = 'Order Finished'
       GROUP BY customer) first
GROUP BY EXTRACT(YEAR FROM  first_order);
