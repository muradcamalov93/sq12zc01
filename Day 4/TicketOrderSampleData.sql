/*

TRUNCATE TABLE PAYMENTS;
TRUNCATE TABLE BOOKINGS ;
TRUNCATE  TABLE TICKETS ;
TRUNCATE TABLE MOVIES ;
TRUNCATE TABLE CUSTOMERS ;
TRUNCATE  TABLE CASHIERS ;
*/


--Add new customers
INSERT INTO CUSTOMERS (id,fname,lname,age,gender,address,phone)  values(1,'Ali','Mehdiyev',19,'M','Binaqadi Baku','050-345-23-42');
INSERT INTO customers (id,fname,lname,age,gender,address,phone)  values(2,'Mahmud','Tayibov',23,'M','4 mkrn Sumqayit','0552344344');


--Add new cashiers
INSERT INTO CASHIERS (id,fname,lname,age,gender,address,phone) values(1,'Hamid','Karimov',34,'M','Yasamal Baku','0701234333');
INSERT INTO CASHIERS (id,fname,lname,age,gender,ADDRESS,phone) VALUES(2,'Ayten','Faridli',23,'F','Abseron Baku','0503424422');

--Add new movies
INSERT INTO MOVIES (id,genre,rating,lang,ccountry,age_restriction) values(1,'Drama',7,'Eng','USA',18);
INSERT INTO movies (id,genre,rating,lang,ccountry,age_restriction) values(2,'Action',4,'Eng','Germany',15);

--Add new tickets
INSERT INTO TICKETS (id,ticket_no,movie_id,start_time,price,expire_date) values(1,'0100012',1,sysdate+3,15.40,sysdate+5);
INSERT INTO TICKETS (id,ticket_no,movie_id,start_time,price,expire_date) values(2,'0100013',1,sysdate+2,15.40,sysdate+5);

--Add new bookings
INSERT INTO BOOKINGS (id,cust_id,ticket_id,booking_date) values(1,1,1,sysdate+1);
INSERT INTO bookings (id,cust_id,ticket_id,booking_date) values(2,2,2,sysdate+2);

--Add new payments
INSERT INTO PAYMENTS (id,booking_id,cashier_id,amount,pay_type,pay_date) values(1,1,1,15.40,'CARD',sysdate+1);
INSERT INTO PAYMENTS (id,booking_id,cashier_id,amount,pay_type,pay_date) values(2,2,2,15.40,'CASH',sysdate+2);

COMMIT;
