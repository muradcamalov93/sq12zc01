CREATE TABLE customers
  (
     id      INT PRIMARY KEY,
     fname   VARCHAR2(35),
     lname   VARCHAR2(35),
     age     INT,
     gender  CHAR(1),
     address VARCHAR2(100),
     phone   VARCHAR2(30)
  );

CREATE TABLE cashiers
  (
     id      INT PRIMARY KEY,
     fname   VARCHAR2(35),
     lname   VARCHAR2(35),
     age     INT,
     gender  CHAR(1),
     address VARCHAR2(100),
     phone   VARCHAR2(30)
  );

CREATE TABLE movies
  (
     id              INT NOT NULL PRIMARY KEY,
     genre           VARCHAR2(30),
     rating          NUMBER(10, 2),
     lang            VARCHAR2(5),
     ccountry        VARCHAR2(30),
     age_restriction NUMBER(10)
  );

CREATE TABLE tickets
  (
     id          INT NOT NULL PRIMARY KEY,
     ticket_no   VARCHAR2(10) NOT NULL,
     movie_id    INT NOT NULL,
     start_time  TIMESTAMP,
     price       NUMBER(10, 2),
     expire_date DATE,
     CONSTRAINT fk_tickets_movie_id FOREIGN KEY(movie_id) REFERENCES movies(id)
  );

CREATE TABLE bookings
  (
     id           INT NOT NULL PRIMARY KEY,
     cust_id      INT NOT NULL,
     ticket_id    INT NOT NULL,
     booking_date TIMESTAMP,
     CONSTRAINT fk_bookings_cust_id FOREIGN KEY (cust_id) REFERENCES customers(id),
     CONSTRAINT fk_bookings_ticket_id FOREIGN KEY (ticket_id) REFERENCES tickets(id)
  );

CREATE TABLE payments
  (
     id         INT NOT NULL PRIMARY KEY,
     booking_id INT NOT NULL,
     cashier_id INT NOT NULL,
     amount     NUMBER(10, 2),
     pay_type   VARCHAR2(10),
     pay_date   TIMESTAMP,
     CONSTRAINT fk_payments_booking_id FOREIGN KEY (booking_id) REFERENCES bookings(id),
     CONSTRAINT fk_payments_cashier_id FOREIGN KEY (cashier_id) REFERENCES cashiers(id)
  ); 
