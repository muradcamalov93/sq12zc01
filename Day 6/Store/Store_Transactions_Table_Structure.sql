create table store_transactions (
	id INT,
	first_name VARCHAR2(50),
	last_name VARCHAR2(50),
	email VARCHAR2(50),
	phone VARCHAR2(50),
	gender VARCHAR2(50),
	country VARCHAR2(50),
	product VARCHAR2(50),
	discount INT,
	price number(5,2),
	count INT,
	order_date date,
	company VARCHAR(50),
	Status VARCHAR(9)
);

